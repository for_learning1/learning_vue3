import { computed, ref } from 'vue'

const useEventSpace = (initCapacity) => {
    const capacity = ref(initCapacity);
    const attending = ref(['Tim', 'Bob', 'Joe'])
    const spacesLeft = computed(() => capacity.value - attending.value.length)
    
    const addMarlon = () => attending.value.push('marlon')
    const increase = () => capacity.value++
    const decrease = () => capacity.value--

    return {
        capacity,
        increase,
        decrease,
        attending,
        spacesLeft,
        addMarlon
    }
}

export default useEventSpace

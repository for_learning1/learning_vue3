import { ref } from 'vue'

const usePromise = (fn) => {
    const results = ref(null)
    const loading = ref(false)
    const error = ref(null)

    const timer = (ms) => {
        return new Promise(res => setTimeout(res, ms));
    }

    const createPromise = async (...args) => {
        loading.value = true
        error.value = null
        results.value = null

        await timer(3000)

        try {
            results.value = await fn(...args)
        } catch (err) {
            error.value = err
        } finally {
            loading.value = false
        }
    }

    return {
        results,
        loading,
        error,
        createPromise
    }
}

export default usePromise

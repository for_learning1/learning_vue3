import { reactive } from 'vue';

const useNamesHandler = () => {
    const form = reactive({
        firstName: '',
        lastName: ''
    })
    return {
        form
    }
}

export default useNamesHandler

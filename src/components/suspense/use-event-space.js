import { ref } from 'vue'

const useEventSpace = async (delay) => {
    const capacity = ref(0);

    const timer = (ms) => {
        return new Promise(res => setTimeout(res, ms));
    }
    
    const increase = () => {
        capacity.value++
    }
    const decrease = () => {
        capacity.value--
    }

    await timer(delay)

    // const a = 1
    // a.push(3)

    return {
        capacity,
        increase,
        decrease,
    }
}

export default useEventSpace

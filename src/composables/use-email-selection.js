import axios from 'axios';
import { reactive } from "vue"

const emails = reactive(new Set())

const useEmailSelection = () => {
    const toggle = (email) => {
        if (emails.has(email)) {
            emails.delete(email)
        } else {
            emails.add(email)
        }
        console.log(emails)
    }
    const clear = () => {
        emails.clear()
    }
    const addMultiple = (newEmails) => {
        newEmails.forEach(email => emails.add(email));
    }
    const forSelected = (fn) => {
        emails.forEach(email => {
            fn(email)
            axios.put(`http://localhost:5011/emails/${email.id}`, email)
        })
    }
    const markRead = () => forSelected(email => email.read = true)
    const markUnread = () => forSelected(email => email.read = false)
    const archive = () => forSelected(email => email.archived = true)
    return {
        emails,
        toggle,
        clear,
        addMultiple,
        markRead,
        markUnread,
        archive,
    }
}

export default useEmailSelection
